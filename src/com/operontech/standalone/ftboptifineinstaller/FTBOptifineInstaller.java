package com.operontech.standalone.ftboptifineinstaller;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class FTBOptifineInstaller extends JFrame {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPane;
	final JButton installButton = new JButton("Install OptiFine");
	final JFileChooser chooser = new JFileChooser();
	final JComboBox<Object> modPackFolderBox = new JComboBox<Object>();
	final JLabel lblModpack = new JLabel("Modpack Folder:");
	final JLabel lblOptifine = new JLabel("OptiFine Location:");
	final JProgressBar progressBar = new JProgressBar();
	final JComboBox<String> optiFineBox = new JComboBox<String>();

	public File downloadedFile;
	private final List<String> listOfItems = new ArrayList<String>();
	private final Map<String, String> nameToPath = new HashMap<String, String>();

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					final FTBOptifineInstaller frame = new FTBOptifineInstaller();
					frame.setVisible(true);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FTBOptifineInstaller() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FTBOptifineInstaller.class.getResource("/com/operontech/standalone/ftboptifineinstaller/icon.png")));
		setResizable(false);
		setTitle("FTB OptiFine Installer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 158);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		installButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (new File(getModPackPath() + File.separator + "minecraft" + File.separator + "mods").exists()) {
					installOptifine(new File(getModPackPath() + File.separator + "minecraft" + File.separator + "mods"));
				} else if (new File(getModPackPath() + File.separator + "mods").exists()) {
					installOptifine(new File(getModPackPath() + File.separator + "mods"));
				} else if (getModPackPath().substring(getModPackPath().length() - 4, getModPackPath().length()).equalsIgnoreCase("mods")) {
					installOptifine(new File((getModPackPath())));
				} else {
					JOptionPane.showMessageDialog(null, "The folder doesn't seem to be an FTB modpack!", "Uh Oh!", JOptionPane.WARNING_MESSAGE);
				}
			}
		});

		installButton.setBounds(109, 85, 125, 23);
		installButton.setEnabled(false);
		contentPane.add(installButton);

		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		modPackFolderBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if (modPackFolderBox.getSelectedItem().toString().equals("Browse...")) {
					if (chooser.showOpenDialog(contentPane) == JFileChooser.APPROVE_OPTION) {
						addToModPackList(chooser.getSelectedFile().getAbsolutePath());
						modPackFolderBox.setSelectedItem(chooser.getSelectedFile().getAbsolutePath());
						installButton.setEnabled(!getModPackPath().equals("") && !getModPackPath().equals("Browse..."));
					}
				}
			}
		});
		modPackFolderBox.setModel(new DefaultComboBoxModel<Object>(new String[] { "", "Browse..." }));
		modPackFolderBox.setBounds(120, 25, 215, 20);
		modPackFolderBox.setMaximumRowCount(5);
		contentPane.add(modPackFolderBox);

		lblModpack.setHorizontalAlignment(SwingConstants.CENTER);
		lblModpack.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblModpack.setBounds(0, 24, 125, 18);
		contentPane.add(lblModpack);

		lblOptifine.setHorizontalAlignment(SwingConstants.CENTER);
		lblOptifine.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOptifine.setBounds(0, 56, 125, 18);
		contentPane.add(lblOptifine);

		optiFineBox.setModel(new DefaultComboBoxModel<String>(new String[] { "Download Latest from DropBox" }));
		optiFineBox.setBounds(120, 57, 215, 20);
		contentPane.add(optiFineBox);
		progressBar.setForeground(Color.GREEN);

		progressBar.setBounds(0, 119, 344, 10);
		contentPane.add(progressBar);

		final JLabel lblPleaseSelectThe = new JLabel("Please select the folder of the modpack (inside FTB folder)");
		lblPleaseSelectThe.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseSelectThe.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPleaseSelectThe.setBounds(0, 0, 344, 23);
		contentPane.add(lblPleaseSelectThe);
		getFTBFolder();
	}

	public void installOptifine(final File folder) {
		try {
			final URL website = new URL("https://dl.dropboxusercontent.com/u/36877135/OptiFine%20Patched.jar");
			downloadedFile = new File(folder.getAbsolutePath() + File.separator + "Optifine Patched.jar");
			downloadedFile.delete();
			downloadedFile.createNewFile();
			new Thread() {
				@Override
				public void run() {
					progressBar.setValue(0);
					progressBar.setMaximum(tryGetFileSize(website));
					installButton.setEnabled(false);
					while (progressBar.getValue() != progressBar.getMaximum()) {
						progressBar.setValue((int) downloadedFile.length());
						if (Math.round((progressBar.getValue() / progressBar.getMaximum()) * 100) == 0) {
							installButton.setText("Downloading...");
						} else {
							installButton.setText(String.valueOf(Math.round((progressBar.getValue() / progressBar.getMaximum()) * 100)) + "%");
						}
					}
					installButton.setText("Install OptiFine");
					installButton.setEnabled(true);
					JOptionPane.showMessageDialog(null, "Installation of OptiFine is complete!", "Installation Complete", JOptionPane.INFORMATION_MESSAGE);
				}
			}.start();
			new Thread() {
				@Override
				public void run() {
					try {
						final ReadableByteChannel rbc = Channels.newChannel(website.openStream());
						final FileOutputStream fos = new FileOutputStream(folder.getAbsolutePath() + File.separator + "Optifine Patched.jar");
						fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
						fos.close();
					} catch (final IOException e) {
						e.printStackTrace();
					}
				}
			}.start();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public String getModPackPath() {
		return nameToPath.get((modPackFolderBox.getSelectedItem()));
	}

	public void getFTBFolder() {
		File dir = new File(defaultDirectory() + File.separator + "ftblauncher" + File.separator + "ftblaunch.cfg");
		if (!dir.exists() && !dir.isDirectory()) {
			return;
		}
		final File originalDir = dir;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(originalDir));
			String line = "";
			while ((line = br.readLine()) != null) {
				if (!line.startsWith("#") && line.contains("=") && line.split("=")[0].equals("installPath")) {
					final StringBuilder string = new StringBuilder();
					for (final String part : line.split("=")[1].split("\\\\")) {
						string.append(part + File.separator);
					}
					dir = new File(string.toString().replace("C\\", "C"));
					if (dir.exists() && dir.isDirectory()) {
						break;
					}
				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		try {
			br.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
		if (dir.equals(originalDir)) {
			return;
		}
		for (final File file : dir.listFiles()) {
			final String name = file.getName();
			if (name == null) {
				return;
			}
			if (!Arrays.asList("assets", "authlib", "libraries", "versions").contains(name.toLowerCase()) && !name.endsWith(".txt")) {
				addToModPackList(file.getAbsolutePath());
			}
		}
		installButton.setEnabled(true);
	}

	private String convertToModPackName(final String s) {
		return (new File(s)).getName();
	}

	private String defaultDirectory() {
		final String OS = System.getProperty("os.name").toUpperCase();
		if (OS.contains("WIN")) {
			return System.getenv("APPDATA");
		} else if (OS.contains("MAC")) {
			return System.getProperty("user.home") + "/Library/Application " + "Support";
		} else if (OS.contains("NUX")) {
			return System.getProperty("user.home");
		}
		return System.getProperty("user.dir");
	}

	private void addToModPackList(final String newPack) {
		listOfItems.clear();
		listOfItems.add(convertToModPackName(newPack));
		nameToPath.put(convertToModPackName(newPack), newPack);
		for (int i = 0; i < modPackFolderBox.getItemCount(); i++) {
			if (!modPackFolderBox.getItemAt(i).equals("Browse...") && !modPackFolderBox.getItemAt(i).equals("") && !modPackFolderBox.getItemAt(i).equals(newPack)) {
				listOfItems.add(String.valueOf(modPackFolderBox.getItemAt(i)));
			}
		}
		listOfItems.add("");
		listOfItems.add("Browse...");
		modPackFolderBox.setModel(new DefaultComboBoxModel<Object>(listOfItems.toArray()));
	}

	private int tryGetFileSize(final URL url) {
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("HEAD");
			conn.getInputStream();
			return conn.getContentLength();
		} catch (final IOException e) {
			return 0;
		} finally {
			conn.disconnect();
		}
	}
}
